# ===========================================================
# MODULE Exp_helper
# ---------------
# included struct definitions:
# - Meta_data
# - Movement
# - Sequence
# - Button
# - Audio
# - Image
# - GVS
# - Gamepad
# - Spnav
# - xsense
# - Login_param
#
# included functions:
# - experiment_banner
# - get_input
# - get_hand
# - next_trial
# - import_logfile
# ===========================================================
# Copyright (C) 2019 Daniel Fitze and Matthias Ertl, University of Bern,
# daniel.fitze@psy.unibe.ch, matthias.ertl@psy.unibe.ch
#
# This file is part of Platform Commander.
#
# Platform Commander is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Platform Commander is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Platform Commander.  If not, see <http://www.gnu.org/licenses/>.
# ===========================================================
module Exp_helper
using Dates, Printf, Questionnaires, CSV, FIGlet
using Base: @kwdef

# ===========================================================
# struct definitions
# ===========================================================
@kwdef struct Meta_data
  exp_ID::String
  exp_conditions::Array{String} = []
  exp_block::Array{Int64} = []
  exp_flag::Array{Bool} = [true]
  exp_date::String = string(Dates.format(Dates.now(), "yyyy-mm-dd"))
  exp_time::String = string(Dates.format(Dates.now(), "HH-MM-SS"))
  subject_ID::String = string(get_input("SubjectID", "string", []))
  subject_Sex::String = string(get_input("Sex", "string", ["m"; "w"]))
  subject_Age::String = string(get_input("Age", "Int", [18;120]))
  subject_Hand::String = get_hand(exp_flag)
  log_filename::String = (exp_ID * "_" * subject_ID * "_" * exp_date * "_" * exp_time)
end

@kwdef struct Movement
  dof::Vector{Float64}
  time::Float64
  law::String = "S"
  max_actuator_speed::Float64 = 0.15
  rotcenter_heave::Float64 = 0.0
  rotcenter_surge::Float64 = 0.0
  rotcenter_lateral::Float64 = 0.0
end

@kwdef struct Sequence # long sequence of motion profiles
  name::String 
  motions::Array
end

@kwdef struct Button
  numberBtn::Integer = 8
  BtnLabels::Vector{String} = ["1", "2", "3", "4", "5", "6", "7", "8"]
end

@kwdef struct Audio
  filename::String = "cow.wav"
  channel::Int = 2
  volume::Float64 = 0.8
  repetitions::Int = 0
end

@kwdef struct Image
  filename::String = "white"
  visor::String = "VIVE"
  x_res::Int = 600
  y_res::Int = 600
  x_pos_offset::Int = 640
  y_pos_offset::Int = 0
  width::Int = 1280
  height::Int = 1024
  x_pos_virt::Array{Float64} = [0.0]
  y_pos_virt::Array{Float64} = [0.0]
  z_pos_virt::Array{Float64} = [-1500.0]
  yaw_virt::Array{Float64} = [0.0]
  pitch_virt::Array{Float64} = [1.5707963]
  roll_virt::Array{Float64} = [0.0]
  width_virt::Array{Int} = [600]
  height_virt::Array{Int} = [600]
  screen_type::String = "F"
end

@kwdef struct GVS
  name::String = "para7"
  device::String = "TRANS-VIRT"
  subdeviceNb::Int = 1
  ports::String = "0|0.0|5.0|6|0.0|10.0"
  frequency::Float64 = 60.0
  channelNb::Int = 0
  order::Int = 0
  reps::Int = 0
end

@kwdef struct Gamepad
  numberBtn::Int = 2
  BtnLabels::Vector{String} = ["fr1", "fl1"]
end

@kwdef struct Spnav
  numberBtn::Int = 2
  BtnLabels::Vector{String} = ["left", "right"]
end

@kwdef struct xsense
  xsense::String = "ja"
end

@kwdef struct Login_param
  mode::String = "D"
  start_height::Float64 = -0.2
  input_device::String = "gamepad"
  visor::String = "VIVE"
  stim_audio::Bool = false
  n_channels::Int = 4
  stim_visual::Bool = false
end

# ===========================================================
# function definitions
# ===========================================================
function experiment_banner(;name, font = "speed", ask = true)
  FIGlet.render(name, font)

  if ask == true
    println("")
    println("Start the experiment [y/n]?")
    println("")
    test = readline()

    if test == "n"
      error("start again")
    elseif test == "y"
      println("")
      println("")
    end
  end
end

function get_input(text, accepted_type = "string", accepted_values = [])
  while true
    print(text,": ")
    inp = readline()
    println(accepted_type)
    if lowercase(accepted_type) == "string"
      if ~isempty(accepted_values)
        tmp = findall(accepted_values -> accepted_values == inp,accepted_values)
        if ~isempty(tmp)
          return inp
        else
          println("only this inputs are allowed: ", accepted_values)
        end
      else
        return inp
      end
    elseif lowercase(accepted_type) == "int"
      try
        inp = parse(Int64, inp)
        if ~isempty(accepted_values)
          if inp >= accepted_values[1] && inp <= accepted_values[2]
            return inp
          else
            println("Input has to be in the range ", accepted_values[1]," - ", accepted_values[2])
          end
        else
          return inp
        end
      catch
        println("")
        println("Input must be an integer!")
      end
    end
  end

end

function get_hand(flag)
  h = "0"
  if flag[1]
    h = Questionnaires.simpleEHI()
  else
    h = get_input("Handedness", "string", ["r";"l";"a"])
  end

  return h
end

function next_trial(conditions, counter, criteria)
  c_diff = counter .- criteria
  ##println("=c_diff=> ", vec(c_diff))
  cand_idx = findall(x->x<0, vec(c_diff))
  #println(cand_idx[:,1])

  if isempty(cand_idx)
    candidate = 0
  else
    #	println("=====> ", cand_idx)
    candidate = cand_idx[rand(1:length(cand_idx))]
  end
  #println(candidate)
  return candidate
end

function import_logfile(filename)
  Situation = CSV.read(filename*".situation.csv")
  Messages = CSV.read(filename*".messages.csv")

  return Situation, Messages
end

 end # module end
