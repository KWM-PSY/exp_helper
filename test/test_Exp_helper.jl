# ===========================================================
# Copyright (C) 2019 Daniel Fitze and Matthias Ertl, University of Bern,
# daniel.fitze@psy.unibe.ch, matthias.ertl@psy.unibe.ch
#
# This file is part of Platform Commander.
#
# Platform Commander is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Platform Commander is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Platform Commander.  If not, see <http://www.gnu.org/licenses/>.
# ===========================================================


using Exp_helper
# =======================
# En-/diable tests
# =======================
auto_test = false # if true skip tests requiering keyboard inputs

do_test_get_device = false
do_test_Meta_data = true
do_test_Meta_data_I = false
do_test_Trial = false
do_test_Movement = false
do_test_Logfile = false 

if do_test_Logfile
    S, M = Exp_helper.import_logfile("/home/docer/Schreibtisch/200107.160155")
    println(S)
end

if do_test_Meta_data 
	A = Exp_helper.Meta_data("test", ["fast" "slow"],[])
    println(A)
    println("exp ID =====> $(A.exp_ID)")
    println("exp conds =====> $(A.exp_conditions)")
    println("exp date =====> $(A.exp_date)")
    println("exp time =====> $(A.exp_time)")
    println("subject ID =====> $(A.subject_ID)")
    println("subject sex =====> $(A.subject_Sex)")
    println("subject age =====> $(A.subject_Age)")
    println("subject hand =====> $(A.subject_Hand)")
    println("log filename =====> $(A.log_filename)")

	A = Exp_helper.Meta_data("test", ["fast" "slow"],[],[false])
    println(A)
    println("exp ID =====> $(A.exp_ID)")
    println("exp conds =====> $(A.exp_conditions)")
    println("exp date =====> $(A.exp_date)")
    println("exp time =====> $(A.exp_time)")
    println("subject ID =====> $(A.subject_ID)")
    println("subject sex =====> $(A.subject_Sex)")
    println("subject age =====> $(A.subject_Age)")
    println("subject hand =====> $(A.subject_Hand)")
    println("log filename =====> $(A.log_filename)")
end

if do_test_Movement
    A = Exp_helper.Movement([0.0, 0.0, 0.0, 0.0, 0.0, 0.0], 2)
    println(A)
    A.time = 5
    println(A)
end
