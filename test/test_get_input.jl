# ===========================================================
# Copyright (C) 2019 Daniel Fitze and Matthias Ertl, University of Bern,
# daniel.fitze@psy.unibe.ch, matthias.ertl@psy.unibe.ch
#
# This file is part of Platform Commander.
#
# Platform Commander is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Platform Commander is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Platform Commander.  If not, see <http://www.gnu.org/licenses/>.
# ===========================================================


using Exp_helper, Printf

if false
    A = Exp_helper.get_input("test")
    println("Output test 1: ", A)
end

if false
    A = Exp_helper.get_input("test", "int")
    println("Output test 2: ", A)
end

if false
    A = Exp_helper.get_input("age", "int", [18 130])
    println("Output test 3: ", A)
end

if true
    A = Exp_helper.get_input("sex", "string", ["m", "f"])
    println("Output test 4: ", A)
end


